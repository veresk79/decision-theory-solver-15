package ru.spb.beavers.modules.hotelrooms;

import java.util.ArrayList;
import java.util.Random;


/**
 * Calculates task 3.2. Hotel rooms
 * @author AntoVita
 *
 */
public class TaskSolver {
	
	public static InputData parse(String M, String Vf, String Vt) {
		InputData result = new InputData();
		try {
			result.M = Double.parseDouble(M);
			result.Vf = Double.parseDouble(Vf);
			result.Vt = Double.parseDouble(Vt);
			
			if (result.M<=0 || result.Vt<=0 || result.Vf<=0 || result.Vf <= result.Vt)
				throw new Exception();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return result;
	}
	
	public static OutputData getResult(InputData input) {
		OutputData result = new OutputData();
		
		double step = input.M / (double)input.countOfVertices;
		result.coordinates = new ArrayList<Point>(); 
		result.d = -1;
		input.Px.setParam(input.M);
		input.Py.setParam(input.M);
		
		double min = Double.MAX_VALUE;
		for (double i = 0; i < input.M; i+=step) {
			double fVf = f(input.Vf, input.Py.f(input.M - i));
			double fVt = f(input.Vt, input.Px.f(i));
			result.coordinates.add(new Point(i, fVf, fVt));
			
			double diff = Math.abs(fVf - fVt);
			if (diff < min) {
				min = diff;
				result.d = (int)(i + 0.5);
				result.Vd = f(input.Vf, input.Py.f(result.d));
			}
		}
		
		return result;
	}
	
	private static double f(double v, double p) {
		return v * (1-p);
	}
}