package ru.spb.beavers.modules.task_3_0_x_components.graph.matrix.operations;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;
import edu.uci.ics.jung.algorithms.matrix.GraphMatrixOperations;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Владимир on 09.04.2015.
 */
public class GraphMatrixConverter {
    public static SparseDoubleMatrix2D graphToMatrixConvert(DirectedSparseGraph<String, String> graph, Map<Integer, String> indexer){
        int numVertices = graph.getVertexCount();
        SparseDoubleMatrix2D matrix = new SparseDoubleMatrix2D(numVertices,
                numVertices);

        for(String v : graph.getVertices())
        {
            for (String e : graph.getOutEdges(v))
            {
                String w = graph.getOpposite(v,e);
                matrix.set(getId(v, indexer), getId(w, indexer), 1.0);
            }

        }
        return matrix;
    }

    private static int getId(String x, Map<Integer, String> indexer){
        for (int i = 0; i < indexer.values().size(); i++) {
            if(indexer.get(i).equalsIgnoreCase(x)){
                return i;
            }
        }
        return -1;
    }

    public static DirectedSparseGraph<String, String> matrixToGraphConvert(DoubleMatrix2D matrix, Map<Integer,String> indexer){
        DirectedSparseGraph<String, String> graph = new DirectedSparseGraph<>();
        for (String vertex : indexer.values()) {
            graph.addVertex(vertex);
        }
        if (matrix.rows() != matrix.columns())
        {
            throw new IllegalArgumentException("Matrix must be square.");
        }

        int size = matrix.rows();
        String edge = "edge";
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                double value = matrix.getQuick(i, j);
                if(value == 1.0){
                    graph.addEdge(edge+" "+(j)+" "+(i), indexer.get(i), indexer.get(j));
                }
            }
        }
        return graph;
    }
}
