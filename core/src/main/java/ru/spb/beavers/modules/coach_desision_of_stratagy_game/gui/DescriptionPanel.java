package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui;

/**
 * Created by Sasha on 09.04.2015.
 */
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import org.apache.commons.collections15.Transformer;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.ElementKord;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.PanelEl;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.RenderDiagram;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.VertexS;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sasha on 09.04.2015.
 */
public class DescriptionPanel {



        private static Map vx_coords;
        private static Map vx_coords2;
        private ElementKord last_el_coords;

        public DescriptionPanel(JPanel panel)
        {
            last_el_coords = new ElementKord();

            panel.setLayout(null);
            panel.setSize(760, 2000);
            panel.setPreferredSize(new Dimension(750, 2000));

            // Неформальная постановка задачи
            PanelEl.setTextHeader("Неформальная постановка задачи", 20, 300, 20, last_el_coords, panel);
            last_el_coords.updateValues(10, 200);
            PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/informal_task_description.html", last_el_coords, panel);

            // Формальная постановка задачи
            PanelEl.setTextHeader("Формальная постановка задачи", 30, 300, 20, last_el_coords, panel);
            last_el_coords.updateValues(10, 325);
            PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/formal_task_description.html", last_el_coords, panel);



            // Диаграмма влияния
            PanelEl.setTextHeader("Диаграмма влияния", 30, 300, 20, last_el_coords, panel);
            createDependencyDiagram(panel);

            // Описание для диаграммы влияния
            JLabel jl = new JLabel("<html><style> p { text-indent: 20px; }</style><body width=\"580\" height=\"100\">"
                    + "<p>D - Множество решений, которое содержит решение сделать тачдаун или забить гол.</p>"
                    + "<p>Y - Случайная величина, которая обределяет получает ли команда очки</p>"
                    + "<p>R - Множество результатов, которое содержит победу, ничью, поражение</p>"
                    + "</body></html>");
            last_el_coords.updateValues(30, 100);
            jl.setSize(600, last_el_coords.height);
            jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, last_el_coords.y);
            panel.add(jl);

            // Дерево решения
            PanelEl.setTextHeader("Дерево решений", 30, 300, 20, last_el_coords, panel);
            PanelEl.setDescriptionTree1(panel, null, last_el_coords, 10);
            // Описание для диаграммы влияния
            JLabel jl2 = new JLabel("<html><style> p { text-indent: 20px; }</style><body width=\"580\" height=\"100\">"
                    + "<p>d1 - Решение забить гол.</p>"
                    + "<p>d2 - Решение забить тачдаун.</p>"
                    + "<p>P11 - Вероятность забить гол</p>"
                    + "<p>P12 - Вероятность неудачи при попытке забить гол.</p>"
                    + "<p>P21 - Вероятность забить тачдаун</p>"
                    + "<p>P22 - Вероятность неудачи при попытке забить тачдаун</p>"
                    + "<p>a1 -  Успешная попытка забить гол.</p>"
                    + "<p>a2 -  Не успешная попытка забить гол.</p>"
                    + "<p>a3 -  Успешная попытка забить тачдаун.</p>"
                    + "<p>a4 -  Не успешная попытка забить тачдаун.</p>"
                    + "<p>L1 -  Лотерея, возникающая в в результате принятия решения d1.</p>"
                    + "<p>L2 -  Лотерея, возникающая в в результате принятия решения d2.</p>"
                    + "</body></html>");
            last_el_coords.updateValues(30, 200);
            jl2.setSize(600, last_el_coords.height);
            jl2.setLocation(panel.getWidth() / 2 - jl2.getWidth() / 2, last_el_coords.y);
            panel.add(jl2);
        }



        public void createDependencyDiagram(JPanel panel)
        {
            int ox = 40;
            int oy = 40;

            vx_coords = new HashMap();

            vx_coords.put("D", new Point2D.Double(ox+70, oy+30));
            vx_coords.put("Y", new Point2D.Double(ox+150, oy+90));
            vx_coords.put("R", new Point2D.Double(ox+220, oy+20));

            ArrayList<VertexS> vx_list = new ArrayList<VertexS>(3);

            vx_list.add(new VertexS("D", "Square")); // 0
            vx_list.add(new VertexS("Y", "Circle")); // 1
            vx_list.add(new VertexS("R", "Rhomb")); // 2

            Graph<VertexS, String> basis = new DirectedSparseMultigraph<VertexS, String>();
            for(VertexS vx: vx_list)
            {
                basis.addVertex(vx);
            }

            basis.addEdge("D-Y", vx_list.get(0), vx_list.get(1));
            basis.addEdge("D-R", vx_list.get(0), vx_list.get(2));

            basis.addEdge("Y-R", vx_list.get(1), vx_list.get(2));

            Transformer<VertexS, Point2D> locationTransformer = new Transformer<VertexS, Point2D>() {

                @Override
                public Point2D transform(VertexS vertex) {
                    Point2D vx_coord = (Point2D)vx_coords.get(vertex.name);
                    return new Point2D.Double((double) vx_coord.getX(), (double) vx_coord.getY());
                }
            };

            StaticLayout<VertexS, String> layout = new StaticLayout<VertexS, String>(
                    basis, locationTransformer);
            edu.uci.ics.jung.visualization.VisualizationViewer<VertexS, String> vv = new edu.uci.ics.jung.visualization.VisualizationViewer<VertexS, String>(layout);

            vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<VertexS, String>());
            vv.getRenderContext().setVertexLabelTransformer(new Transformer<VertexS, String>() {
                                                                public String transform(VertexS vx) {
                                                                    return vx.name;
                                                                }
                                                            }
            );
            vv.getRenderer().setVertexRenderer(new RenderDiagram());
            vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.N);
            vv.setSize(315, 190);
            last_el_coords.updateValues(10, vv.getHeight());
            vv.setLocation(panel.getWidth() / 2 - vv.getWidth() / 2, last_el_coords.y);
            panel.add(vv);
        }








}
