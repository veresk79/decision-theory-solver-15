package ru.spb.beavers.modules.seats_plane;

/**
 * Created by Nikita on 10.04.2015.
 */
public class TaskCommon {
    public static final String GLOBAL_TITLE = "3.1.1 Задача о распределении\nресурсов";
    public static final String DESC_TITLE = "<html><h2 align='center'>Неформальное описание задачи</h2></html>";
    public static final String FORMAL_TITLE = "Формальная постановка задачи";
    public static final String USEFULL_TITLE = "Расчет полезностей";
    public static final String SOLUTION_TITLE = "Решение";

    public static final String SPROS_CLASS_TYPE = "Тип спроса: ";
    public static final String SPROS_CLASS_1 = "Спрос на билеты I класса";
    public static final String SPROS_CLASS_2 = "Спрос на билеты II класса";
    public static final String SPROS_SIZE =  "Величина спроса:                   ";
    public static final String COUNT_PLACE = "Количество мест(M):              ";
    public static final String PRICE_CLASS_1 = "Стоимость билетов I класса: ";
    public static final String PRICE_CLASS_2 = "Стоимость билетов II класса: ";


    public static final String DIAGRAMM_NAME = "Диаграмма влияния нашей задачи";
    public static final String DESC_MESSAGE = "Пусть авиакомпания продает билеты " +
            "на плановый рейс самолета. Имеющиеся места в самолете разделяются на" +
            "места первого или второго классов. Стоимость мест второго класса сущест" +
            "венно ниже стоимости мест первого класса, и, следовательно, спрос на места" +
            "первого класса также ниже. Распределение мест первого и второго классов" +
            "осуществляется до момента начала продажи билетов и в дальнейшем " +
            "скорректировано быть не может. Также предполагается, что никто из пасса" +
            "жиров купленные на рейс билеты не сдает. Доход авиакомпании определя" +
            "ется стоимостью проданных билетов. В случае если авиакомпания принимает " +
            "решение выделить мест под первый класс меньше, чем спрос на первый " +
            "класс, то авиакомпания не получит части дохода, который определяется ко" +
            "личеством пассажиров, которым не хватило билетов на места первого класса. " +
            "В случае если авиакомпания принимает решение выделить мест под первый" +
            "класс больше, чем спрос на места первого класса, то часть билетов останутся" +
            "не проданными и авиакомпания понесет убытки. Задача сводится к выбору" +
            "соотношения между местами первого и второго классов, при котором" +
            "авиакомпания получит максимальный доход от продажи билетов.";

    public static final String FORMAL_MESSAGE = "<html><h2 align='center'>Решение</h2><p>" +
            "Множество решений D = {d | , 0<=d<=M},<br/> " +
            "где d - решение выделить d мест под месте II класса; M - общее количество мест в самолете<br/>" +
            "Тогда под места I класса выделено (M-d) мест. Стоимость билетов I класса равна v<sub>f</sub>,<br/>" +
            "стоимость билетов на места I класса равна v<sub>d</sub><br/>" +
            "Покупательский спрос на места II класса описывается непрерывной случайной величиной X,<br/>" +
            "принимающей значения из множества X={ x | 0<=x }, где x - спрос на х - мест второго класса. Случайная<br/> " +
            "величина X описывает плотность распредления p(x) на интервале [0, M].<br/>" +
            "Покупательский спрос на места I класса описывается непрерывной случайной величиной Y, принимающей значения <br/>" +
            "из множества Y={ y | 0<=y }, где y -   спрос на y мест первого класса<br/>"+
            "Случайная величина Y описывается плотностью <br/>" +
            "распределения p(y) y на интервале [0, M]. X , Y – представляют собой независимые случайные величины.<br/>" +
            " Принятое решение описывается величиной <i>D</i>, принимающей значения из множества D.<br/><br/>" +
            "<body>\n" +
            "  <center><table border=\"1\">\n" +
            "   <caption>Возможные значения функции ценности</caption>\n" +
            "   <tr>\n" +
            "    <th>V (X, Y, d)</th>\n" +
            "    <th>y &lt M-d</th>\n" +
            "    <th>y &gt= M-d</th>\n" +
            "   </tr>\n" +
            "   <tr><td>x &lt= d</td><td>v<sub>d</sub>x + v<sub>f</sub>y</td><td>v<sub>d</sub>x + v<sub>f</sub>(M-d)</td></tr>\n" +
            "   <tr><td>x &gt d</td><td>v<sub>d</sub>d + v<sub>f</sub>y</td><td>v<sub>d</sub>d + v<sub>f</sub>(M-d)</td></tr>" +
            "  </table></center>\n" +
            " </body>"+
            "</html>";

    public static final String PIC_PATH = "task_3_1/";
    public static final String PIC_AIRPLANE ="airplane.jpg";
    public static final String PIC_FORMAL = "task_3_1_1_diagramm.png";
    public static final String PIC_USEFULL = "task_3_1_1_useful.png";
    public static final String PIC_SOLUTION = "task_3_1_1_solution.png";
    public static final String PIC_RESOLVE_1 = "task_3_1_1_solve_1.png";
    public static final String PIC_RESOLVE_2 = "task_3_1_1_solve_2.png";
    public static final String PIC_RESOLVE_USE_FORMULA = "task_3_1_1_formulaUse.png";
    public static final String PIC_RESOLVE_USE_FORMULA2 = "task_3_1_1_formulaUse2.png";


}
