package ru.spb.beavers.modules;

import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;

import static edu.uci.ics.jung.visualization.Layer.LAYOUT;


//Ширина 850
public class ProblemMilk implements ITaskModule {

	private class SaveListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0)
        {
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("2.1.3"));
				bw.write(Vs_text.getText() + "\n");
				bw.write(Vc_text.getText() + "\n");
				bw.write(M_text.getText() + "\n");
				bw.write(A_text.getText() + "\n");
				bw.write(S_text.getText() + "\n");
				bw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
        }
    }
	
	private class LoadListener implements ActionListener {
	    @Override 
		public void actionPerformed(ActionEvent arg0)
	    {
	    	try {
				BufferedReader br = new BufferedReader(new FileReader("2.1.3"));
				Vs_text.setText(br.readLine());
				Vc_text.setText(br.readLine());
				M_text.setText(br.readLine());
				A_text.setText(br.readLine());
				S_text.setText(br.readLine());			
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}
	
	private class DefaultValuesListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			Vs_text.setText("10");
			Vc_text.setText("8");
			M_text.setText("40");
			A_text.setText("20");
			S_text.setText("10");				
		}
    }
	

	private SaveListener saveListener;
	private LoadListener loadListener;
	private DefaultValuesListener defaultValuesListener;
	
	private double Vs; //цена продажи
	private double Vc; //цена закупа
	private double M; //максимальный спрос на молоко
	private double A; //параметр нормального распределения
	private double S; //параметр нормального распределения
	private int Raspr; //0 - нормальное распределение, 1 - логнормальное нормальное, 2 - равномерное 
	
	double max_i; //Байесовское решение
	double Ud; //полезность Байесовского решения
	double Ut; //полезность точной информации

	JTextField Vs_text;
	JTextField Vc_text;
	JTextField M_text;
	JComboBox raspr_box;
	JTextField A_text;
	JTextField S_text;
	
	public ProblemMilk()
	{
		A = 15;
		S = 0.5;
		Raspr = 2;
	}
	
	public void setParams(double a, double s, int raspr)
	{
		A = a; S =  s; Raspr = raspr;
	}
		
	private double integrate(double a, double b, int iterations) //P
	{
		double s = 0;
		for(int i = 0; i < iterations; i++)
			s += p(i*(b-a)/iterations);
		return s*(b-a)/iterations;
	}
	
	public double Ey(int iterations)
	{
		double s = 0;
		for(int i = 0; i < iterations; i++)
		{
			double x = i*M/iterations;
			s += x*p(x);
		}
		return s*M/iterations;
	}
	
	private double integratePb(double a/*0*/, double b/*d**/, int iterations) //Для расчета байесовской полезности
	{
		double s = 0;
		double Pd = (Vs - Vc) / Vs;
		for(int i = 0; i < iterations; i++)
		{
			double x = i*(b-a)/iterations;
			s += x*p(x)/Pd;
		}
		return s*(b-a)/iterations;
	}
	
	/**
	 * Интеграл от функции распределения
	 * @param a
	 * @param b
	 * @param iterations
	 * @return
	 */
	private double integrateP(double a/*0*/, double b/*d*/, int iterations)
	{
		double s = 0;
		for(int i = 0; i < iterations; i++)
			s += P(b, i * (b - a) / iterations);
		return s*(b-a)/iterations;
	}
	
	/**
	 * Функция распределения
	 * @param d
	 * @param y
	 * @return
	 */
	private double P(double d, double y)
	{
		return (d-y)*p(y);
	}
	
	/**
	 * Функция плотности
	 * @param y - аргумент
	 * @return - значение функции
	 */	
	private double p(double y)
	{
		switch(Raspr)
		{
		case 0:
			return Math.exp(-Math.pow(y-A, 2)/(2*Math.pow(S, 2)))/(S*Math.sqrt(2*Math.PI));
		case 1:
		//	if(y == 0) return 0;
		//	return Math.exp(-Math.pow(Math.log(y)-A, 2)/(2*Math.pow(S, 2)))/(y*S*Math.sqrt(2*Math.PI));
		case 2:
			return 1/M;
		}
		return 0;
	}
	
	private double U(double d)
	{
		return (Vs - Vc)*d - Vs*integrateP(0, d, 5000);
	}
	
	private double Ud(double d)
	{
		return (Vs - Vc)*integratePb(0, d, 5000);
	}
	
	private double Ut()
	{
		return (Vs - Vc)*Ey(5000);
	}
	
	public void solve(double _Vs, double _Vc, double _M)//Точность M до 1го знака после запятой
	{
		Vs = _Vs;
		Vc = _Vc;
		M = _M;
		
		//Находим U(d)
		double[] u = new double[(int) (10*M)];

		for(double i = 0; i < M; i+=0.1)
		{	
			u[(int) (10*i)] = U(i);	//Определяем полезности решений
		}
			 
		//Байесовский подход (ищем максимум)
		max_i = 0; //это d*
		for(double i = 0; i < M; i+=0.1)
			if(u[(int) (10*i)] > u[(int) (10*max_i)])
				max_i = i; 

		//Полезность Байесовского решения
		Ud = Ud(max_i);
		
		//Полезность точной информации
		//1) Полезность решения d**
		Ut = Ut();
	}

	@Override
	public String getTitle()
	{
		// TODO Auto-generated method stub
		return "2.1.3 Задача о молоке";
	}

	@Override
	public void initDescriptionPanel(JPanel panel) 
	{
		// TODO Auto-generated method stub
		JLabel taskDescription = new JLabel(""
				+ "<html>Пусть предприятие занимается продажей молока. С округи скупаются<br>"
				+ "запасы у фермеров, которые затем хранятся в цистерне. На следующий<br>"
				+ "день молоко продается конечным покупателям. Если за день предприятию<br>"
				+ "не удается продать молоко, то оно портится. Возможности использовать<br>"
				+ "испорченное молоко у предприятия нет. Доход предприятия определяется<br>"
				+ "количеством проданного молока и разницей между его оптовой и розничной<br>"
				+ "стоимостью. Если предприятие закупает недостаточно молока и спрос на<br>"
				+ "него превышает имеющееся количество молока, то предприятие не получит<br>"
				+ "части дохода, который могло бы получить, закупив больше молока. Если<br>"
				+ "предприятие закупает слишком много молока и часть его остается не проданной,<br>"
				+ "предприятие несет потери, равные оптовой стоимости непроданного молока.<br>"
				+ "Задача сводится к выбору количества молока, закупка и продажа которого<br>"
				+ "принесет предприятию наибольший доход.</html>");
		

		panel.add(taskDescription);
	}

	@Override
	public void initSolutionPanel(JPanel panel) 
	{
		// TODO Auto-generated method stub

		BufferedImage myPicture = null;
		try {
			myPicture = ImageIO.read(ProblemMilk.class.getResource("problem_milk/zom.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
        assert myPicture != null;
		JLabel picLabel = new JLabel(new ImageIcon(myPicture));

		panel.setPreferredSize(new Dimension(850, 4400));
		panel.add(picLabel);

	}

	@Override
	public void initInputPanel(JPanel panel) 
	{
		// TODO Auto-generated method stub

		JLabel info = new JLabel("Введите исходные данные для решения задачи.");
		info.setSize(300, 25);
		info.setLocation(50, 10);

		JLabel Vs_info = new JLabel("Цена продажи");
		Vs_info.setSize(200, 25);
		Vs_info.setLocation(50, 50);
		
		Vs_text = new JTextField();
		Vs_text.setSize(200, 25);
		Vs_text.setLocation(300, 50);
		
		JLabel Vc_info = new JLabel("Цена закупа");
		Vc_info.setSize(200, 25);
		Vc_info.setLocation(50, 100);
		
		Vc_text = new JTextField();
		Vc_text.setSize(200, 25);
		Vc_text.setLocation(300, 100);
		
    	JLabel M_info = new JLabel("Максимальный спрос на молоко");
		M_info.setSize(200, 25);
		M_info.setLocation(50, 150);
		
		M_text = new JTextField();
		M_text.setSize(200, 25);
		M_text.setLocation(300, 150);

		String[] raspr = {
			/*"Логарифмическое",*/ "Нормальное", "Равномерное"
		};
		JLabel raspr_text = new JLabel("Вид распределения");
		raspr_text.setSize(200, 25);
		raspr_text.setLocation(50, 200);

		raspr_box = new JComboBox(raspr);
		raspr_box.setSize(200, 25);
		raspr_box.setLocation(300, 200);

		JLabel A_info = new JLabel("Параметр А");
		A_info.setSize(200, 25);
		A_info.setLocation(50, 250);

		A_text = new JTextField();
		A_text.setSize(200, 25);
		A_text.setLocation(300, 250);

		JLabel S_info = new JLabel("Параметр Sigma");
		S_info.setSize(200, 25);
		S_info.setLocation(50, 300);

		S_text = new JTextField();
		S_text.setSize(200, 25);
		S_text.setLocation(300, 300);

		panel.setLayout(null);

		panel.add(info);
		panel.add(Vs_info);
		panel.add(Vs_text);
		panel.add(Vc_info);
		panel.add(Vc_text);	
		panel.add(M_info);
		panel.add(M_text);
		panel.add(raspr_text);
		panel.add(raspr_box);
		panel.add(A_info);
		panel.add(A_text);
		panel.add(S_info);
		panel.add(S_text);

	}

	@Override
	public void initExamplePanel(JPanel panel) 
	{
		// TODO Auto-generated method stub
//		System.out.println("Raspr: " + raspr_box.getSelectedIndex());
		switch (raspr_box.getSelectedIndex())
		{
			case 0:
				Raspr = 0;
				A = Double.parseDouble(A_text.getText());
				S = Double.parseDouble(S_text.getText());
				break;
			case 1:

			case 2:
				Raspr = 2;
				break;
		}

		solve(Double.parseDouble(Vs_text.getText()), Double.parseDouble(Vc_text.getText()), Double.parseDouble(M_text.getText()));

		Chart2D plotn = new Chart2D();
		plotn.setSize(300, 250);
		plotn.setLocation(25, 100);

		plotn.getAxisX().setTitle("");
		//plotn.getAxisX().setVisible(false);
		plotn.getAxisY().setTitle("");
		ITrace2D trace = new Trace2DSimple();
		ITrace2D d_trace0 = new Trace2DSimple();
		d_trace0.setName("d*");
		d_trace0.setColor(Color.green);
		plotn.addTrace(d_trace0);
		plotn.addTrace(trace);
		trace.setColor(Color.blue);
		trace.setName("График плотности");
		d_trace0.addPoint(max_i, 0);
		d_trace0.addPoint(max_i, p(max_i));



		Chart2D polezn = new Chart2D();
		polezn.setSize(300, 250);
		polezn.setLocation(450, 100);
		polezn.getAxisX().setTitle("");
		polezn.getAxisY().setTitle("");

		//polezn.getAxisX().setVisible(false);
		ITrace2D polezn_l = new Trace2DSimple();
		//polezn.addTrace(axisX);
		polezn.addTrace(polezn_l);
		polezn_l.setColor(Color.blue);
		polezn_l.setName("График полезности решений");
		ITrace2D d_trace1 = new Trace2DSimple();
		polezn.addTrace(d_trace1);
		d_trace1.setName("d*");
		d_trace1.setColor(Color.green);
		d_trace1.addPoint(max_i, 0);
		d_trace1.addPoint(max_i, U(max_i));

		for (double i = 0; i <= M; i += 0.1)
		{
			//График плотности
			trace.addPoint(i, p(i));
			//График полезности
			polezn_l.addPoint(i, U(i));
			//График условной плотности
		//	ysl_pl.addPoint(i, Vs*p(i)/(Vs-Vc));
		}


		JLabel answer_info = new JLabel("<html>Решение Байесовским подходом. Лучше всего купить " + /*Math.round(100*max_i)/100*/Math.rint(100*max_i)/100 +
				" литров молока. Полезность решения: " + /*Math.round(100*Ud)/100*/Math.rint(100*U(max_i))/100 + ".<br>Полнзность точной информации " + /*Math.round(100*Ut)/100*/Math.rint(100*Ut)/100 + ".</html>");
		answer_info.setHorizontalAlignment(JLabel.LEFT);
		answer_info.setVerticalAlignment(JLabel.TOP);

		answer_info.setSize(800, 200);
		answer_info.setLocation(25, 10);

		panel.setLayout(null);
		panel.add(answer_info);
		panel.add(plotn);
		panel.add(polezn);

		final DelegateTree<Integer, String> g = new DelegateTree<>();

		g.setRoot(0);

		for(int i = 1; i <= M; i++) {
			g.addChild(String.valueOf(i - 1), 0, 3*i - 2);
			g.addChild("y>" + String.valueOf(i - 1), 3*i - 2, 3*i -1);
			g.addEdge("y<" + String.valueOf(i - 1), 3 * i - 2, 3 * i);
		}

		TreeLayout layout = new TreeLayout<>(g);

		VisualizationViewer vv = new VisualizationViewer<>(layout);
		vv.setSize(800, 500);

		vv.getRenderContext().setVertexFillPaintTransformer(new Transformer<Integer, Paint>() {
			public Paint transform(Integer i) {
				if (i == 0) return Color.GREEN;
				if (i % 3 == 1) return Color.RED;
				return Color.WHITE;
			}
		});

		vv.getRenderContext().setVertexLabelTransformer(new Transformer<Integer, String>() {
			@Override
			public String transform(Integer i) {
				if (i == 0) return "d";
				if (i % 3 == 1) return "y";
				if (i % 3 == 2)
					return String.valueOf(Math.rint(100 * (1 - integrate(0, g.getParent(i) - 1, 1000))) / 100) + "  dV=" + String.valueOf(Vs - Vc);
				else
					return String.valueOf(Math.rint(100 * integrate(0, g.getParent(i) - 1, 1000)) / 100) + "  dV=" + String.valueOf(-Vs);
			}
		});

		vv.getRenderContext().setEdgeLabelTransformer(new Transformer<String, String>() {
			@Override
			public String transform(String edgeName) {
				if (isInteger(edgeName))
					return "d=" + edgeName;
				else return edgeName;
			}
		});

		vv.getRenderContext().setEdgeDrawPaintTransformer(new ConstantTransformer<>(Color.YELLOW));
		vv.getRenderContext().setArrowDrawPaintTransformer(new ConstantTransformer<>(Color.YELLOW));
		vv.getRenderContext().setArrowFillPaintTransformer(new ConstantTransformer<>(Color.YELLOW));
		vv.getRenderContext().setVertexDrawPaintTransformer(new ConstantTransformer<>(Color.WHITE));

		vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);


		//vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT).setScale(1.5, 1.5, vv.getCenter());

		vv.getRenderContext().getMultiLayerTransformer().getTransformer(LAYOUT).rotate(3*Math.PI/2, vv.getCenter());
/*
		DefaultModalGraphMouse graphMouse = new DefaultModalGraphMouse();
		graphMouse.setMode(edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode.PICKING);
		vv.setGraphMouse(graphMouse);*/

		DefaultModalGraphMouse gm = new DefaultModalGraphMouse();
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		vv.setGraphMouse(gm);

		GraphZoomScrollPane scrollPane = new GraphZoomScrollPane(vv);
		scrollPane.setSize(800, 500);
		scrollPane.setLocation(20, 375);

		panel.add(scrollPane);

		panel.setPreferredSize(new Dimension(850, 900));
	}

	@Override
	public ActionListener getPressSaveListener()
			throws IllegalArgumentException {
		// TODO Auto-generated method stub
		if(saveListener == null) {
			saveListener = new SaveListener();
		}
		return  saveListener;
	}

	@Override
	public ActionListener getPressLoadListener()
			throws IllegalArgumentException {
		// TODO Auto-generated method stub
		if(loadListener == null) {
			loadListener = new LoadListener();
		}
		return loadListener;
	}

	@Override
	public ActionListener getDefaultValuesListener()
			throws IllegalArgumentException {
		if (defaultValuesListener == null) {
            defaultValuesListener = new DefaultValuesListener();
        }
        return defaultValuesListener;
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch(NumberFormatException e) {
			return false;
		} catch(NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
}
