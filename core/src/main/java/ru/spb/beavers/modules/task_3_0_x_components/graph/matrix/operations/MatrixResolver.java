package ru.spb.beavers.modules.task_3_0_x_components.graph.matrix.operations;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

/**
 * Created by Владимир on 13.04.2015.
 */
public class MatrixResolver {
    public static DoubleMatrix2D matrixToInverseMatrix(DoubleMatrix2D matrix){
        DoubleMatrix2D suppMatrix = new SparseDoubleMatrix2D(matrix.columns(),  matrix.rows());
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                double k1 = matrix.getQuick(i,j);
                double k2 = matrix.getQuick(j,i);
                suppMatrix.setQuick(i, j, k2);
                suppMatrix.setQuick(j,i, k1);

            }
        }
        return suppMatrix;
    }
    public static DoubleMatrix2D matrixToSuppMatrix(DoubleMatrix2D matrix){
        DoubleMatrix2D suppMatrix = new SparseDoubleMatrix2D(matrix.columns(),  matrix.rows());
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                double k = matrix.getQuick(i,j);
                if(k == 0){
                    suppMatrix.setQuick(i, j, 1.0);
                }else{
                    suppMatrix.setQuick(i,j, 0.0);
                }
            }
        }
        return suppMatrix;
    }

    public static DoubleMatrix2D matrixToIndifferenceMatrix(DoubleMatrix2D matrix){
        matrix = matrixToInverseMatrix(matrix);
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                if(matrix.getQuick(i,j) != matrix.getQuick(j,i)){
                    matrix.setQuick(i,j, 0);
                    matrix.setQuick(j,i, 0);
                }
            }
        }
        return matrix;
    }
    public static String checkMatrix(DoubleMatrix2D matrix){
        String answer = "Проверим матрицу графа на ассиметричность.\n";

        boolean c1 = checkAsymmetry(matrix);
        if(c1){
            answer+="Матрица является ассиметричной!\n";
        }else {
            answer += "Матрица не является ассиметричной!\n";
        }


        boolean c2 = checkAntireflexive(matrix);
        if(c2){
            answer+="Матрица является антирефлексивной!\n";
        }else {
            answer += "Матрица не является антирефлексивной!\n";
        }


        boolean c3 = checkTransitivity(matrix);
        if(c3){
            answer+="Матрица является транзитивной!\n";
        }else {
            answer += "Матрица не является транзитивной!\n";
        }

        boolean c4 = checkHalfTransitivity(matrix);
        if(c4){
            answer+="Матрица является полутранзитивной!\n";
        }else {
            answer += "Матрица не является полутранзитивной!\n";
        }

        if(c1 && c2 && c3 && c4){
            answer += "Матрица является отношением полупорядка!\n";
        }else{
            answer += "Матрица не является отношением полупорядка!\n";
        }
        return answer;
    }
    private static boolean checkAsymmetry(DoubleMatrix2D matrix){
        double k1 = 0.0;
        double k2 = 0.0;
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                k1 = matrix.getQuick(i,j);
                k2 = matrix.getQuick(j, i);
                if(k1 == k2){
                    if(k1 == 1.0){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static boolean checkAntireflexive(DoubleMatrix2D matrix){
        double k1 = 0.0;
        for (int i = 0; i < matrix.rows(); i++) {
            k1 = matrix.getQuick(i,i);
            if (k1 == 1.0) return false;
        }
        return true;
    }

    private static boolean checkTransitivity(DoubleMatrix2D matrix){
        double k1 = 0.0;
        double k2 = 0.0;
        double k3 = 0.0;
        double k4 = 0.0;
        for (int i = 0; i < matrix.columns(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                for (int k = 0; k < matrix.columns(); k++) {

                    if(i != j && i != k && j != k){
                        k1 = matrix.getQuick(i,j);
                        k2 = matrix.getQuick(j,k);
                        if(k1 == 1.0 && k2 == 1.0){
                            if(matrix.getQuick(i,k) != 1.0){ return false;}
                        }
                    }
                }
            }
        }
        return true;
    }

    private static boolean checkNegaTransitivity(DoubleMatrix2D matrix){
        double k1 = 0.0;
        double k2 = 0.0;
        double k3 = 0.0;
        double k4 = 0.0;
        for (int i = 0; i < matrix.columns(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                for (int k = 0; k < matrix.columns(); k++) {

                    if(i != j && i != k && j != k){
                        k1 = matrix.getQuick(i,j);
                        k2 = matrix.getQuick(j,k);
                        if(k1 == 0.0 && k2 == 0.0){
                            if(matrix.getQuick(i,k) != 0.0){ return false;}
                        }
                    }
                }
            }
        }
        return true;
    }

    private static boolean checkLowFullness(DoubleMatrix2D matrix){
        double k1 = 0.0;
        for (int i = 0; i < matrix.columns(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                if(i != j){
                    k1 = matrix.getQuick(i,j);
                    if(k1 == 0.0){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    static{
        System.exit(1);
    }
    private static boolean checkHalfTransitivity(DoubleMatrix2D matrix){
        double k1 = 0.0;
        double k2 = 0.0;
        double k3 = 0.0;
        double k4 = 0.0;
        for (int i = 0; i < matrix.columns(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                for (int k = 0; k < matrix.columns(); k++) {

                    if(i != j && i != k && j != k){
                        k1 = matrix.getQuick(i,j);
                        k2 = matrix.getQuick(j,k);
                        if(k1 == 1 && k2 == 1){
                            for(int z = 0; z < matrix.columns(); z++){
                                if(z != i && z != j && z != k){
                                    if(!(matrix.getQuick(i,z) == 1.0 || matrix.get(z, k) == 1.0)){
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
