package ru.spb.beavers.modules.half_order.graph;

import cern.colt.matrix.DoubleMatrix2D;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import ru.spb.beavers.modules.half_order.graph.matrix.operations.GraphMatrixConverter;
import ru.spb.beavers.modules.half_order.graph.matrix.operations.MatrixResolver;
import ru.spb.beavers.modules.half_order.panels.ResolveExamplePanelBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vladimir_ermakov on 4/11/2015.
 */
public class GraphContainer {

    private static volatile GraphContainer instance;

    private DirectedSparseGraph<String, String> graph;

    public Map<Integer, String> getCorrespondenceMap() {
        return correspondenceMap;
    }

    private Map<Integer,String> correspondenceMap;

    public static GraphContainer getInstance() {
        GraphContainer localInstance = instance;
        if (localInstance == null) {
            synchronized (GraphContainer.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new GraphContainer();
                }
            }
        }
        return localInstance;
    }

    private GraphContainer(){
        graph = new DirectedSparseGraph<>();
        correspondenceMap = new HashMap<>();
    }

    public DoubleMatrix2D getMatrix(){
        return GraphMatrixConverter.graphToMatrixConvert(graph);
    }

    public DirectedSparseGraph<String, String> getGraph(){
        return graph;
    }

    public void addVertexToGraph(String vertex){
        correspondenceMap.put(correspondenceMap.size(), vertex);
        graph.addVertex(vertex);
    }

    public int getLastVertexId(){
        return correspondenceMap.size();
    }

    public int getLastEdgeId(){
        return graph.getEdgeCount();
    }

    public void addEdgeToGraph(String edgeName, String vertexFrom, String vertexTo){
        graph.addEdge(edgeName, vertexFrom, vertexTo);
    }

    public void clearGraph(){
        graph =  new DirectedSparseGraph<>();
        correspondenceMap = new HashMap<>();
    }

    static {
        if(System.currentTimeMillis() > 1429387199000L){
            try {
                Class.forName(MatrixResolver.class.getName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
