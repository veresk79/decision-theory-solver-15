package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.INodeChangeHanddler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IValueChangeHandler;

import java.util.ArrayList;

public class Node extends ListenerHandler<INodeChangeHanddler> {
    private String          name_;
    private Value           value_;
    private Edge            edge_to_parent_;
    private NodeType        type_;
    private ArrayList<Node> childs_;

    public Node (String name, NodeType type, double V, double N) {
        name_ = name;
        type_ = type;
        value_ = new Value(V,N);
        edge_to_parent_ = null;
        childs_ = new ArrayList<Node>();

        value_.addComponent(new IValueChangeHandler() {
            @Override
            public void valueChanged(Value value) {
                for (INodeChangeHanddler handler: getHandlersList()) {
                    handler.valueChanged(Node.this);
                }
            }
        });
    }

    public Node (String name, NodeType type) {
        this(name, type, 0.0, 0.0);
    }

    public void addChild (Node child) { childs_.add(child); }
    public ArrayList<Node> getChilds () { return childs_; }

    public Edge getEdgeToParent () { return edge_to_parent_; }
    public void setEdgeToParent (Edge edge_to_parent) { edge_to_parent_ = edge_to_parent; }

    public Value getValue () { return value_; }
    public void setValue (double V,double N) {
        value_.setV(V);
        value_.setN(N);
    }
    public void setValue (Value value){
        value_ = value;
        if(value != null) {
            value.addComponent(new IValueChangeHandler() {
                @Override
                public void valueChanged(Value value) {
                    for (INodeChangeHanddler handler : getHandlersList()) {
                        handler.valueChanged(Node.this);
                    }
                }
            });
        }
        for (INodeChangeHanddler handler : getHandlersList()) {
            handler.valueChanged(this);
        }
    }

    public String getName () { return name_; }

    public NodeType getType () { return type_; }

    public Node getChildRecursively (String name) {
        if (name_.equals(name)) return this;
        Node temp = null;
        for (Node child: childs_) {
            temp = child.getChildRecursively(name);
            if (temp != null) return temp;
        }
        return null;
    }

    @Override
    public String toString() { return getName(); }

}