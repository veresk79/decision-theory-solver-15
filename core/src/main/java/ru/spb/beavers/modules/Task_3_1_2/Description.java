package ru.spb.beavers.modules.Task_3_1_2;

import com.sun.istack.internal.Nullable;
import com.sun.javafx.beans.annotations.NonNull;
import ru.spb.beavers.modules.Task_3_1_1.attachCapable;
import ru.spb.beavers.modules.Task_3_1_1.creatComponentAsist;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Никита on 10.04.15.
 */
public class Description implements attachCapable {

    private JPanel panel = new JPanel();
    public Description(@Nullable String titleDesc, @Nullable String message, @Nullable String pictureResource)
    {
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.removeAll();
        setDescrptTitle(panel, titleDesc);
        panel.add(Box.createRigidArea(new Dimension(0, 10)));
        creatComponentAsist.setPicture(panel, pictureResource);
        panel.add(Box.createRigidArea(new Dimension(0, 10)));
        setDescrptMessage(panel, message);
    }

    private void setDescrptTitle(JPanel panel, String title)
    {
        JLabel titleLabel = new JLabel(title);
        titleLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        panel.add(titleLabel);
    }

    private void setDescrptMessage(JPanel panel, String message)
    {
        JTextArea descriptionTextArea = new JTextArea();
        descriptionTextArea.setBounds(0, 0, 600, 500);
        descriptionTextArea.setLineWrap(true);
        descriptionTextArea.setWrapStyleWord(true);
        descriptionTextArea.setText(message);
        panel.add(descriptionTextArea);
    }

    @Override
    public void attachToExPanel(@NonNull JPanel exPanel)
    {
        exPanel.removeAll();
        exPanel.add(panel);
    }

}
